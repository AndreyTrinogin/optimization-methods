#ifndef TransportTaskSolverIncluded_M8A3D9K2

#include <vector>

#pragma once

class TransportTaskSolver
{
public:
	TransportTaskSolver();
	~TransportTaskSolver();
	
	/*
	[in] int argc - number of arguments from command line
	[in] char* argv[] - array of string, also is from command line


	Method for solving transport task
	Waiting arguments in argv
	1) file with task
	2) if this position is epmty, solution will be printed on the screen
	   else - will be printed in file with name from argv[2]

	*/
	void solveTask(int argc, char* argv[]);
	
private:
	int sizeNeeds;
	int sizeDeliveries;

	const int MEMORY_RESERVE = 2;

	int* needs;
	int* deliveries;

	std::vector<int> u;
	std::vector<int> v;

	std::vector<std::pair<int, int>> cycle;

	enum {
		RIGHT,
		TOP, 
		LEFT,
		DOWN
	} directions;

	/*
		isFill - is basic cell
		cost - cost of delivery from i to j place
		optLevel - 0, 1 or 2 points by dual preference method
		freight - result of task solving
	*/
	typedef struct cell {
		int isFill;
		int cost;
		int optLevel;
		int freight;
	} cell;

	/*
		table for every stage of program
		Initial Approximation, solution, all of it is here
	*/
	cell** table; 

	/*
		[in] char* fileName - name oe file with 2 number and table for task
	*/
	void readTaskFromFile(char* fileName);

	/*
		[in] char* str - string for logging
	*/
	void logStr(const char* const str);

	/*
		Checking type of task, closed or not
		Comparsion sums of need and deliveries
	*/
	bool isClosedTask(void);

	/* 
		Create initial approximation by dual preference method
	*/
	void createInitialApprox();

	void prioritization(void);

	void fillByOptLevel(void);

	void improveCurPlan(void);

	void calcPotential(void);
	void calcHorzPotential(int num);
	void calcVertPotential(int num);

	std::pair<int, int> findNonOptCell(void);
	int calcTotalCost(void);

	void findCycle(std::pair<int, int> startCell);
	bool findHorzCycle(std::pair<int, int> curCell);
	bool findVertCycle(std::pair<int, int> curCell);


protected:
	void printArray(const int* const vec, const int size);
	void printMatrix(cell** matrix, int field_num);
};

#endif // !TransportTaskSolverIncluded_M8A3D9K2
