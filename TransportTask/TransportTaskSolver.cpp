
#include "TransportTaskSolver.h"
#include <fstream>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <algorithm>

#pragma warning(disable:4996)

TransportTaskSolver::TransportTaskSolver()
{
	time_t curTime;
	time(&curTime);
	logStr("\nStart program at time:");
	logStr(ctime(&curTime));
}

TransportTaskSolver::~TransportTaskSolver()
{
	if (needs)
		delete[] needs;
	if (deliveries)
		delete[] deliveries;
	for (int i = 0; i < sizeDeliveries + MEMORY_RESERVE; i++)
		if (table[i])
			delete[] table[i];

	if (table)
		delete[] table;


	time_t curTime;
	time(&curTime);
	logStr("Finish program at time:");
	logStr(ctime(&curTime));
	logStr("");
}

void TransportTaskSolver::logStr(const char* const str)
{
	std::ofstream file;
	file.open("TransportTaskLog.txt", std::ofstream::out | std::ofstream::app);
	file << str << std::endl;
	file.close();
}

/* Only for me */
void TransportTaskSolver::printArray(const int* const vec, const int size)
{
	for (int i = 1; i <= size; i++)
		std::cout << vec[i] << " ";
	std::cout << std::endl;
	std::cout << std::endl;
}

/* Only for me */
void TransportTaskSolver::printMatrix(cell** matrix, int field_num)
{
	switch (field_num)
	{
		case 0:
		{
			for (int i = 1; i <= sizeDeliveries; i++)
			{
				std::cout.setf(std::ios::showpos);
				for (int j = 1; j <= sizeNeeds; j++)
					std::cout << matrix[i][j].isFill << " ";
				std::cout << std::endl;
				std::cout.unsetf(std::ios::showpos);
			}
			break;
		}
		case 1:
		{
			for (int i = 1; i <= sizeDeliveries; i++)
			{
				for (int j = 1; j <= sizeNeeds; j++)
					std::cout << matrix[i][j].cost << " ";
				std::cout << std::endl;
			}
			break;
		}
		case 2:
		{
			for (int i = 1; i <= sizeDeliveries; i++)
			{
				for (int j = 1; j <= sizeNeeds; j++)
					std::cout << matrix[i][j].optLevel << " ";
				std::cout << std::endl;
			}
			break;
		}
		case 3:
		{
			for (int i = 1; i <= sizeDeliveries; i++)
			{
				for (int j = 1; j <= sizeNeeds; j++)
					if(matrix[i][j].isFill == 1)
						std::cout << std::setw(3) << matrix[i][j].freight << " ";
					else
						std::cout << "[0]"  << " ";
				std::cout << std::endl;
			}
			break;
		}
		default:
			break;
	}
	std::cout << std::endl;
}

void TransportTaskSolver::solveTask(int argc, char* argv[])
{
	if (argc < 2 || argc > 3)
	{
		logStr("Unexpected number of arguments for program");
		return;
	}

	readTaskFromFile(argv[1]);

	printArray(deliveries, sizeDeliveries);
	printArray(needs, sizeNeeds);
	printMatrix(table, 1);

	if (!isClosedTask())
	{
		std::cout << "Task is open, need to close" << std::endl;
		/* here smthg for closing task */
	}
	
	createInitialApprox();

	improveCurPlan();

	auto totalCost = calcTotalCost();

	std::cout << "result = " << totalCost << std::endl;

	if (argc == 2)
	{
		/* print to default file */
	}
	else 
	{
		/* print to file from argv[2] */
	}
}

void TransportTaskSolver::readTaskFromFile(char* fileName)
{
	using std::cin;
	using std::cout;
	using std::endl;

	std::ifstream file(fileName);

	if (file.is_open())
		logStr("Ok, file for reading is open");
	else
	{
		logStr("Smtng wrong with file for reading");
		return;
	}

	file >> sizeDeliveries >> sizeNeeds;
	needs = new(std::nothrow) int[sizeNeeds + MEMORY_RESERVE];

	if (!needs)
	{
		logStr("Error: not enough memory");
		return;
	}
	deliveries = new(std::nothrow) int[sizeDeliveries + MEMORY_RESERVE];
	if (!deliveries)
	{
		logStr("Error: not enough memory");
		return;
	}

	table = new(std::nothrow) cell*[sizeDeliveries + MEMORY_RESERVE];
	if (!table)
	{
		logStr("Error: not enough memory");
		return;
	}

	for (int i = 0; i < sizeDeliveries + MEMORY_RESERVE; i++)
	{
		table[i] = new(std::nothrow) cell[sizeNeeds + MEMORY_RESERVE];
		if (!table[i])
		{
			logStr("Error: not enough memory");
			return;
		}
	}


	for (int i = 0; i < sizeDeliveries; i++)
	{
		file >> deliveries[i + 1];
		for (int j = 1; j <= sizeNeeds; j++)
		{
			file >> table[i + 1][j].cost;
			table[i + 1][j].isFill = 0;
		}
			
	}

	for (int j = 1; j <= sizeNeeds; j++)
		file >> needs[j];

	file.close();
}

bool TransportTaskSolver::isClosedTask(void)
{
	int sum = 0;

	for (int i = 1; i <= sizeDeliveries; i++)
		sum += deliveries[i];
	for (int i = sizeNeeds; i >= 1; i--)
		sum -= needs[i];

	return sum == 0 ? true : false;
}

void TransportTaskSolver::createInitialApprox(void)
{
	prioritization();
	std::cout << "Dual Preference result " << std::endl;
	printMatrix(table, 2);
	fillByOptLevel();

	std::cout << "Supportring plan cell stats " << std::endl;
	printMatrix(table, 0);
	std::cout << "Supportring plan freights " << std::endl;
	printMatrix(table, 3);
	

}

void TransportTaskSolver::prioritization(void)
{
	for (int i = 1; i <= sizeDeliveries; i++)
	{
		// searching min and filling field optLevel
		int min = INT_MAX;
		for (int j = 1; j <= sizeNeeds; j++)
		{
			table[i][j].optLevel = 0;
			if (table[i][j].cost < min)
				min = table[i][j].cost;
		}
		for (int j = 1; j <= sizeNeeds; j++)
		{
			if (table[i][j].cost <= min)
				table[i][j].optLevel++;
		}
	}

	for (int j = 1; j <= sizeNeeds; j++)
	{
		int min = INT_MAX;
		for (int i = 1; i <= sizeDeliveries; i++)
		{
			if (table[i][j].cost < min)
				min = table[i][j].cost;
		}
		for (int i = 1; i <= sizeDeliveries; i++)
		{
			if (table[i][j].cost <= min)
				table[i][j].optLevel++;
		}
	}
}

void TransportTaskSolver::fillByOptLevel(void)
{
	/* local copies */
	int* deliveriesLocal = nullptr;
	int* needsLocal = nullptr;


	deliveriesLocal = new(std::nothrow) int[sizeDeliveries + MEMORY_RESERVE];
	needsLocal = new(std::nothrow) int[sizeNeeds + MEMORY_RESERVE];

	for (int i = 1; i <= sizeDeliveries; i++)
		deliveriesLocal[i] = deliveries[i];

	for (int i = 1; i <= sizeNeeds; i++)
		needsLocal[i] = needs[i];

	for (int n = 2; n >= 0; n--)
	{
		for (int i = 1; i <= sizeDeliveries; i++)
		{
			for (int j = 1; j <= sizeNeeds; j++)
			{
				if (table[i][j].optLevel == n && table[i][j].isFill == 0)
				{
					int tmp = std::min(deliveriesLocal[i], needsLocal[j]);

					deliveriesLocal[i] -= tmp;
					if (deliveriesLocal[i] == 0)
					{
						for (int k = 1; k <= sizeNeeds; k++)
							if (table[i][k].isFill == 0)
								table[i][k].isFill = -1;
					}

					needsLocal[j] -= tmp;
					if (needsLocal[j] == 0)
					{
						for (int k = 1; k <= sizeDeliveries; k++)
							if (table[k][j].isFill == 0)
								table[k][j].isFill = -1;
					}

					table[i][j].freight = tmp;
					table[i][j].isFill = 1;

					if (deliveriesLocal[i] == 0 && needsLocal[j] == 0)
					{
						if (i < sizeDeliveries)
						{
							table[i + 1][j].freight = 0;
							table[i + 1][j].isFill = 1;
						}
						else
						{
							if (i < sizeDeliveries && j < sizeNeeds)
							{
								table[i - 1][j].freight = 0;
								table[i - 1][j].isFill = 1;
							}
						}
					}	
				}
			}
		}
	}

	delete[] needsLocal;
	delete[] deliveriesLocal;

}

static void printVector(std::vector<int> &array, size_t size)
{
	for (size_t i = 1; i <= size; i++)
		std::cout << array[i] << "\t";
	std::cout << std::endl;
}

void TransportTaskSolver::calcPotential(void)
{
	u.clear();
	v.clear();

	for (int i = 0; i <= sizeDeliveries + 1; i++)
		u.push_back(-1);

	for (int i = 0; i <= sizeNeeds + 1; i++)
		v.push_back(-1);

	u[1] = 0;
	calcHorzPotential(1);
	printVector(u, sizeDeliveries);
	printVector(v, sizeNeeds);

}

void TransportTaskSolver::calcHorzPotential(int num)
{
	for (int j = 1; j <= sizeNeeds; j++)
	{
		if (v[j] != -1)
			continue;
		if (table[num][j].isFill != 1)
			continue;

		v[j] = table[num][j].cost + u[num];
		calcVertPotential(j);
	}
}

void TransportTaskSolver::calcVertPotential(int num)
{
	for (int i = 1; i <= sizeDeliveries; i++)
	{
		if (u[i] != -1)
			continue;
		if (table[i][num].isFill != 1)
			continue;

		u[i] = v[num] - table[i][num].cost;
		calcHorzPotential(i);
		
	}
}

void TransportTaskSolver::improveCurPlan(void)
{
	std::pair<int, int> nonOptCell;

	while (true)
	{
		calcPotential();
		nonOptCell = findNonOptCell();
		if (nonOptCell.first != -1 && nonOptCell.second != -1)
		{
			findCycle(nonOptCell);
		}
		else
			break;
	}

}

std::pair<int, int> TransportTaskSolver::findNonOptCell(void)
{
	std::pair<int, int> res = { -1, -1 };

	for (int i = 1; i <= sizeDeliveries; i++)
		for (int j = 1; j <= sizeNeeds; j++)
		{
			if (table[i][j].isFill != -1)
				continue;
			auto delta = table[i][j].cost - (v[j] - u[i]);
			std::cout << "delta[" << i << "]" << "[" << j << "]"
				<< " = " << delta << std::endl;
			if (delta < 0)
			{
				res = {i, j};
				return res;
			}
		}
	return res;
}

int TransportTaskSolver::calcTotalCost(void)
{
	int res = 0;
	for(auto i = 1; i <= sizeDeliveries; i++)
		for (auto j = 1; j < sizeNeeds; j++)
			if (table[i][j].isFill == 1)
				res += table[i][j].cost * table[i][j].freight;
			
	return res;
}

void TransportTaskSolver::findCycle(std::pair<int, int> startCell)
{
	cycle.clear();
	if (findHorzCycle(startCell))
	{

	}
}


bool TransportTaskSolver::findHorzCycle(std::pair<int, int> curCell)
{

}

bool TransportTaskSolver::findVertCycle(std::pair<int, int> curCell)
{

}