#ifndef MINIMIZER_H_INCLUDED
#define MINIMIZER_H_INCLUDED
#pragma once

#include <string>

class Minimizer
{
public:

	Minimizer();
	~Minimizer();

	void SetValues(double right_b, double left_b, double eps, double(*function)(double x));

	void SetValues(double right_b, double left_b, double eps, double(*function)(double x), std::string resFileName);

	double DichotomyMin(void);
	double GoldenRatioMin(void);

private:
	bool isInit;
	bool isInitFile;

	double a;
	double b;
	double eps;
	double(*func)(double x);
	std::string fileName;

	bool isCorrectVal(void);
	void PrintFile(std::string str);
	void PrintRes(double al, double bl, double res, int hitCount);

};

#endif // !MINIMIZER_H_INCLUDED
