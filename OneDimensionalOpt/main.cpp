
#define _USE_MATH_DEFINES

#include "Minimizer.h"
#include <cmath>
#include <vld.h>
#include <iostream>

// main task
double function(double x)
{
	return log(1 + x * x) - sin(x);
}

// my test function
double ezfunction(double x)
{
	return sin(x);
}

int main(void)
{
	Minimizer minz;
	double a = 0.0;
	double b = M_PI / 4.0;
	double eps = 0.1;
	double min_f;

	minz.SetValues(a, b, eps, function, "log.txt");
	
	min_f = minz.DichotomyMin();
	//std::cout << min_f << std::endl;

	minz.GoldenRatioMin();
	//std::cout << min_f << std::endl;

	return 0;
}