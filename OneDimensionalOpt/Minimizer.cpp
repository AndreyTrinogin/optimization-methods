
#pragma warning(disable:4996)

#include "Minimizer.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>

Minimizer::Minimizer()
{
	this->isInit = false;
	this->isInitFile = false;
}

Minimizer::~Minimizer()
{
	time_t curTime;
	time(&curTime);
	PrintFile("Program finish at: ");
	PrintFile(ctime(&curTime));
}

void Minimizer::SetValues(double right_b, double left_b, double epsilon, double(*function)(double x))
{
	this->a = right_b;
	this->b = left_b;
	this->eps = epsilon;
	this->func = function;
	this->isInit = true;
}

void Minimizer::SetValues(double right_b, double left_b, double epsilon, double(*function)(double x), std::string resFileName)
{
	SetValues(right_b, left_b, epsilon, function);
	this->fileName += resFileName;
	this->isInitFile = true;

	time_t curTime;
	time(&curTime);
	PrintFile("\nSet values at: ");
	PrintFile(ctime(&curTime));
}

void Minimizer::PrintFile(std::string str)
{
	if (this->isInitFile)
	{
		std::ofstream file(this->fileName, std::ios::app);
		file << str << std::endl;
		file.close();
	}
}

void Minimizer::PrintRes(double al, double bl, double res, int hitCount)
{
	if (this->isInitFile)
	{
		std::ofstream file(this->fileName, std::ios::app);
		file.setf(std::ios::fixed);
		file << std::setprecision(10);
		file << "\n on [" << this->a << "; " << this->b << "]";
		file << " your function has min in [" << al << "; " << bl << "]" << std::endl;
		file << "interval length is " << abs(bl - al) <<std::endl;
		file << "our values is " << res << std::endl;
		file << "function value in point " << res << " is " << this->func(res) << std::endl;
		file << "number of computing fucntion is " << hitCount << std::endl;
		file.close();
	}
}

bool Minimizer::isCorrectVal(void)
{
	if (!this->isInit)
	{
		std::cout << "Error: values is not initialized" << std::endl;
		PrintFile("Error: values is not initialized");
		return false;
	}
	if (a >= b)
	{
		std::cout << "Error: values is not correct" << std::endl;
		PrintFile("Error: values is not correct");
		return false;
	}

	return true;
}

double Minimizer::DichotomyMin(void)
{
	if (!isCorrectVal())
	{
		std::cout << "Result is incorrect" << std::endl;
		PrintFile("Result is incorrect");
		return 0.0;
	}
		
	int hitCount = 0;
	double delta = eps / 2.0 - 0.1 * eps /2.0;
	double bl = b; /* bl, al -  locals */
	double al = a;
	double x; // left from middle
	double y; // right from middle
	double fx;
	double fy;

	while (bl - al > eps)
	{
		y = (al + bl) / 2.0 + delta;
		x = y - 2.0 * delta;
		fx = func(x);
		fy = func(y);
		hitCount += 2;
		if (fx < fy)
			bl = y;
		else
			al = x;
	}
	PrintFile("\nBy DichotomyMin method");
	PrintRes(al, bl, (al + bl) / 2.0, hitCount);

	return (al + bl) / 2.0;
}

double Minimizer::GoldenRatioMin(void)
{
	if (!isCorrectVal())
	{
		std::cout << "Result is incorrect" << std::endl;
		PrintFile("Result is incorrect");
		return 0.0;
	}

	int hitCount = 0;
	double bk = b; /* bl, al -  locals */
	double ak = a;

	double lambda; // left from middle
	double mu; // rigth from middle
	double flambda;
	double fmu;
	double alpha = (3.0 - sqrt(5)) / 2.0;

	if (bk - ak < eps)
	{
		PrintRes(ak, bk, (ak + bk) / 2.0, hitCount);
		return (ak + bk) / 2.0;
	}

	lambda = ak + alpha * (b - a);
	mu = bk - alpha * (b - a);
	flambda = func(lambda);
	fmu = func(mu);
	hitCount += 2;
	while (bk - ak > eps) 
	{
		if (flambda < fmu)
		{
			bk = mu;
			mu = lambda;
			fmu = flambda;
			lambda = ak + alpha * (bk - ak);
			flambda = func(lambda);
		}
		else
		{
			ak = lambda;
			lambda = mu;
			flambda = fmu;
			mu = bk - alpha * (bk - ak);
			fmu = func(mu);
		}
		hitCount++;
	}

	PrintFile("\nBy GoldenRatioMin method");
	PrintRes(ak, bk, (ak + bk) / 2.0, hitCount);

	return (ak + bk) / 2.0;
}
